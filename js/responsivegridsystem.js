<!-- hide from old browsers

var days = new Array();
days[0] = "Sunday";
days[1] = "Monday";
days[2] = "Tuesday";
days[3] = "Wednesday";
days[4] = "Thursday";
days[5] = "Friday";
days[6] = "Saturday";

var months = new Array();
months[0] = "January";
months[1] = "February";
months[2] = "March";
months[3] = "April";
months[4] = "May";
months[5] = "June";
months[6] = "July";
months[7] = "August";
months[8] = "September";
months[9] = "October";
months[10] = "November";
months[11] = "December";

// end script hiding -->


function reduce_font_size_to_fit(inner, outer) {
    'use strict';
    // reduce fontsize until inner.height() < outer.height()...

    var height = 0;
    var zone_height = $(outer).height();
    var i = 100;

    height = inner.height();

    if ( height > zone_height ) {
        for (i=100; i>10;i-=3){

            height = inner.height();
            if (height <= zone_height) {
                console.log ('reducing font size to ' + i + '%');
                break;
            }
            inner.css('font-size', i + '%');
        }
        console.log(i)
    }
}


