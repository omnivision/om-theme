/**
 * Functionality specific to OM 2014.
 *
 * Provides helper functions to enhance the theme experience.
 */

( function( $ ) {
	"use strict";
	var body    = $( 'body' ),
	    _window = $( window );

	/**
	 * Adds a top margin to the footer if the sidebar widget area is higher
	 * than the rest of the page, to help the footer always visually clear
	 * the sidebar.
	 */
	$( function() {
		if ( body.is( '.sidebar' ) ) {
			var sidebar   = $( '#secondary .widget-area' ),
			    secondary = ( 0 == sidebar.length ) ? -40 : sidebar.height(),
			    margin    = $( '#tertiary .widget-area' ).height() - $( '#content' ).height() - secondary;

			if ( margin > 0 && _window.innerWidth() > 999 )
				$( '#colophon' ).css( 'margin-top', margin + 'px' );
		}
	} );

	/**
	 * Enables menu toggle for small screens.
	 */
	( function() {
		var nav = $( '#site-navigation' ), button, menu;
		if ( ! nav )
			return;

		button = nav.find( '.menu-toggle' );
		if ( ! button )
			return;

		// Hide button if menu is missing or empty.
		menu = nav.find( '.nav-menu' );
		if ( ! menu || ! menu.children().length ) {
			button.hide();
			return;
		}

		$( '.menu-toggle' ).on( 'click.om2014', function() {
			nav.toggleClass( 'toggled-on' );
		} );
	} )();

	/**
	 * Makes "skip to content" link work correctly in IE9 and Chrome for better
	 * accessibility.
	 *
	 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
	 */
	_window.on( 'hashchange.om2014', function() {
		var element = document.getElementById( location.hash.substring( 1 ) );

		if ( element ) {
			if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) )
				element.tabIndex = -1;

			element.focus();
		}
	} );

	/**
	 * Arranges footer widgets vertically.
	 */
	if ( $.isFunction( $.fn.masonry ) ) {
		var columnWidth = body.is( '.sidebar' ) ? 228 : 245;

		$( '#secondary .widget-area' ).masonry( {
			itemSelector: '.widget',
			columnWidth: columnWidth,
			gutterWidth: 20,
			isRTL: body.is( '.rtl' )
		} );
	}



	// make hover/icon/zone things look correct.
        $('.omarea').hover( function () {
            $(this).children('.desc').css('opacity',1);
        }, function() {
            $(this).children('.desc').css('opacity',0);
        }).map(function() {
            //reduce_font_size_to_fit($(this).children('.desc').first(), $(this));
            //reduce_font_size_to_fit($(this).children('.title').first(), $(this));
        });


	// scrollbox 'plugin'

        $.fn.scrollbox = function (show_time) {
            var scrollbox = this;

            scrollbox.scrollbox_stop = false;

             scrollbox.hover(function(){scrollbox.scrollbox_stop=true;},
                             function(){scrollbox.scrollbox_stop=false;}).css('opacity', 1)
                      .slideDown('fast');

                 scrollbox.children().first().addClass('scrollbox_shown').siblings().css('display', 'none');

            setInterval(function() {

                var height = scrollbox.height();

                if (scrollbox.scrollbox_stop) { return; }

                // don't keep growing and shrinking. that's annoying.

                if (height > parseInt(scrollbox.css('min-height'), 10)) {
		    scrollbox.css('min-height', height);
                }

            // swap which children are visible
            if (scrollbox.children().length > 1) { 
            scrollbox.children('.scrollbox_shown').removeClass('scrollbox_shown').fadeOut(1000,
                function () {
            
                    var next = $(this).next();
                    if (next.length){
                        next.addClass('scrollbox_shown').fadeIn();
                    } else {
                        scrollbox.children().first().addClass('scrollbox_shown').fadeIn();
                    }
                });
	    }
            }, show_time ? show_time : 7000);
            
        };


} )( jQuery );
